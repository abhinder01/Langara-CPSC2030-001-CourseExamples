function parsePixelString(pixelString) {
    let strippedString = pixelString.replace("px","  ");
    return Number(strippedString);
}

setInterval(()=>{
   let container = $(".animated_banner")[0];
   let widthString = window.getComputedStyle(container).width;
   let width = parsePixelString(widthString);

   let newElement = $("<img>",{
      src :"img/droplet.png",
      class: "rain"
   })
   
   //One of the few places we set CSS in javascript
   //because position is different for each element
   newElement.css("left", Math.floor(Math.random()*width)+"px");
   //Have to added event handler seperately or event object doesn't come
   //back correclty
   newElement.bind("animationend", (event)=>{
       $(event.target).remove();
   })
   newElement.appendTo(container);
   
},5)