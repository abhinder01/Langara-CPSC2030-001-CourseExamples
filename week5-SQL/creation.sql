create table  stores (
    store text,
    loc_x int,
    loc_y int,
    phone text   
);

create table storeinventory (
    store text,
    item text,
    price float,
    inventory float
);

create table warehouse (
    item text,
    price float,
    inventory float,
    description text
);

Insert into stores values("Store A",1,1,"111-1111");
Insert into stores values("Store B",2,5,"222-2222");
Insert into stores values("Store C",6,10,"666-6666");
Insert into stores values("Store D",2,7,"333-3333");
Insert into stores values("Store E",4,5,"444-4444");

Insert into storeinventory values ("Store A","Widget A",10,0);
Insert into storeinventory values ("Store A","Widget B",20,12);
Insert into storeinventory values ("Store A","Widget C",15,13);
Insert into storeinventory values ("Store A","Widget D",22,55);
Insert into storeinventory values ("Store A","Widget E",35,23);
Insert into storeinventory values ("Store B","Widget A",11,2);
Insert into storeinventory values ("Store B","Widget B",14,4);
Insert into storeinventory values ("Store B","Widget C",23,5);
Insert into storeinventory values ("Store C","Widget W",11,10);
Insert into storeinventory values ("Store C","Widget A",10,2);
Insert into storeinventory values ("Store C","Widget C",17,90);
Insert into storeinventory values ("Store C","Widget D",12,1);
Insert into storeinventory values ("Store D","Widget B",18,23);
Insert into storeinventory values ("Store D","Widget C",17,90);
Insert into storeinventory values ("Store D","Widget D",12,1);
Insert into storeinventory values ("Store E","Widget B",14,4);
Insert into storeinventory values ("Store E","Widget C",23,5);
Insert into storeinventory values ("Store E","Widget F",10,23);
Insert into storeinventory values ("Store E","Widget G",11,12);

Insert into warehouse values ("Widget A",1,100,"Amazing red thing");
Insert into warehouse values ("Widget B",2,200,"Awesome green object");
Insert into warehouse values ("Widget C",3,300,"Terrific yellow item");
Insert into warehouse values ("Widget D",4,245,"Marvelous orange entity");
Insert into warehouse values ("Widget E",5,126,"Something really terrible");


