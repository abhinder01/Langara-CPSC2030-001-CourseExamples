/*Demonstrates left join */
/*For this query we want to see if the store
  is selling any items that is not in the warehouse
  -For those items we expect one item to have value
  -second item column to not have value */

  select store, storeinventory.item, warehouse.item
  from storeinventory
  left join warehouse on storeinventory.item = warehouse.item
  where warehouse.item is null;
