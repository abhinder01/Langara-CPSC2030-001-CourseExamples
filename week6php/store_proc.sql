/* one function to get all the stores */
DELIMITER //
CREATE PROCEDURE store_list ()
BEGIN
  SELECT store, loc_x, loc_y
  From stores;
END //
DELIMITER ;


/*one function to get what the stores sells */
DELIMITER //
CREATE PROCEDURE merch_table
(IN str text)
BEGIN
select store, storeinventory.item, storeinventory.price, warehouse.description
from storeinventory
inner join warehouse on storeinventory.item = warehouse.item
where store = str;

END //
DELIMITER ;
