function createClickHandler(menuElement) {

    var element = menuElement;
    const expandClass = "expand";
    //Event handler
    return  (event) =>{
        if (event.target == element.querySelector(".title")) {
            if (menuElement.className == expandClass) {
                menuElement.className = "";
            } else {
                menuElement.className = expandClass;
            }
        }
    }
}
function createMenuEventHandler(target, menu){
    var element = target;
    const expandClass = "menu expand";
    //Event handler
    return  (event) =>{
        if (event.target == target) {
            if (menu.className == expandClass) {
                menu.className = "menu";
            } else {
                menu.className = expandClass;
            }
        }
    }
}

function setupMenu(containerName) {
    var menus = document.querySelectorAll(containerName + ">div");

    menus.forEach((element)=>{
        element.onclick = createClickHandler(element);
    });
    
    var target = document.querySelector(containerName + ">.target");
    var menu = document.querySelector(containerName);
    target.onclick = createMenuEventHandler(target,menu );


}
setupMenu(".menu");